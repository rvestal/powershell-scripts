## Reset Windows Update counters

Write-Host ""
Write-Host ""
Write-Host "Stopping Windows Updates and BITS"
Stop-Service wuauserv
Stop-Service bits

Write-Host ""
Write-Host ""
Write-Host "Removing SoftwareDistribution.bak if it exists"

if (Test-Path -Path C:\Windows\SoftwareDistribution.bak) {
   Remove-Item -Path C:\Windows\SoftwareDistribution.bak -Recurse -Confirm:$false
}

Write-Host ""
Write-Host ""
Write-Host "Renaming C:\Windows\SoftwareDistribution folder"
Rename-Item C:\Windows\SoftwareDistribution C:\Windows\SoftwareDistribution.bak

Write-Host ""
Write-Host ""
Write-Host "Starting Windows Updates and BITS"
Start-Service wuauserv
Start-Service bits

Write-Host ""
Write-Host ""
Write-Host "Forcing Windows Updates to reset authorization and"
Write-Host "redetect needed updates"
wuauclt /resetauthorization /detectnow
wuauclt /reportnow
