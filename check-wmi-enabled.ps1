# Check if WMI is enabled
# Author: Roy Vestal

$myArray = "server1","server2","server3","server4"

foreach ($varSystem in $myArray) {
    Get-WmiObject -Computername $varSystem -class Win32_ComputerSystem >> c:\wmi-systems.txt
    }
