# Search for large files in users folders

# Path to search
$varPath = "C:\Users"

# What the minimum file size to be searched
$varSize = 1MB

# Limit number of rows - if needed un-remark
#$varLimit = 5

# search for a specific extension - if needed un-remark
#$varExtention = "*.iso"

##script to find the files all options
$varFiles = get-ChildItem -path $varPath -recurse -ErrorAction "SilentlyContinue" -include $varExtension | ? { $_.GetType().Name -eq "FileInfo" } | where-O
bject {$_.Length -gt $varSize} | sort-Object -property length -Descending | Select-Object Name, @{Name="SizeInMB";Expression={$_.Length / 1MB}},@{Name="Pat
h";Expression={$_.directory}} -first $varLimit
$varFiles


# script to find all files of $varSize
$varFiles = get-ChildItem -path $varPath -recurse -ErrorAction "SilentlyContinue"  | ? { $_.GetType().Name -eq "FileInfo" } | where-Object {$_.Length -gt $
varSize} | sort-Object -property length -Descending | Select-Object Name, @{Name="SizeInMB";Expression={$_.Length / 1MB}},@{Name="Path";Expression={$_.dire
ctory}}
$varFiles
