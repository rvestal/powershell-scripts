## Remove files older than 30 days
## If you would like to change the date, change the maxDays variable

# Maximum days allowed
$maxDays = 30

# Path to search
$varPath = "C:\Users"


$folderName = Get-ChildItem -Directory C:\Users | Where-Object {$_.Name}
$folderName = $folderName -replace '\s',''


ForEach ($account in $folderName)
{

    # Get the FolderNames of each folder in $varPath
    $testDir = Get-ChildItem -Directory $varPath | Where-Object {$_.Name -eq $account}
    $homeDir = "c:\Users\$testDir"

    If (($account -ne "Public") -and ($account -ne "Administrator"))
    {
       #echo "$homeDir to be cleaned"
      Get-ChildItem $varPath -Recurse -File | Where-Object {$_.LastWriteTime -lt (Get-Date).AddDays(-$maxDays)} | Remove-Item -Force
      
       #Write-EventLog -LogName "Windows PowerShell" -Source "PowerShell" -EventId 1001 -EntryType Information -Message "$homeDir cleaned"
      

    }
    Else
    {
        echo "$account Bypassed"
    }
 }
