# This script updates the registry for the Microsoft ADV190013 "Microsoft Windows Server Registry Key Configuration Missing"
# This will require each system to be rebooted before the fix is loaded.
# Depending on your Windows version you may need to adjust the values of the reg add lines below for your Windows OS version
# See the Micro$oft advisory at the below link for more details
# https://portal.msrc.microsoft.com/en-us/security-guidance/advisory/ADV190013

# I recommend running this script 2 times. The first time is to run the reg query statements below to verify if the fix is needed.
# If it is needed, I recommend update the servers.txt file with the hosts that need it, and then run the reg add lines to update.

# Get the server names of each server from a servers.txt file
#foreach($compName in Get-Content C:\servers.txt) {

# Print the hostname of the server being tested/updated
#$compName


# Query the registry keys to verify the settings
#Invoke-Command -ComputerName $compName -ScriptBlock {reg query "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management" /v FeatureSettingsOverride}

#Invoke-Command -ComputerName $compName -ScriptBlock {reg query "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management" /v FeatureSettingsOverrideMask}

#echo " "

# Add the registry keys to apply the fix. This will require a manual reboot
#Invoke-Command -ComputerName $compName -ScriptBlock {reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management" /v FeatureSettingsOverride /t REG_DWORD /d 72 /f}

#Invoke-Command -ComputerName $compName -ScriptBlock {reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management" /v FeatureSettingsOverrideMask /t REG_DWORD /d 3 /f}

#}
