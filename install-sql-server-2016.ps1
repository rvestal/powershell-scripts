# Download and install Microsoft SQL Server Express 2016 and install taking default settings

# Download the software 
Write-Host ""
Write-Host ""
Write-Host "Downloading Microsoft SQL Server Express 2016"

$url = "https://download.microsoft.com/download/3/7/6/3767D272-76A1-4F31-8849-260BD37924E4/SQLServer2016-SSEI-Expr.exe"
$output = "C:\Users\Administrator\Downloads\SQLServer2016-SSEI-Expr.exe"
$start_time = Get-Date

Invoke-WebRequest -Uri $url -OutFile $output
Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s)"

#Run the installer quiet and take all defaults. Wait for installer to complete before continuing.

Write-Host ""
Write-Host ""
Write-Host ""

start-process "C:\Users\Administrator\Downloads\SQLServer2016-SSEI-Expr.exe" -ArgumentList "/IAcceptSqlServerLicenseTerms /Quiet" -wait

#Delete installer after installation is complete

Delete C:\Users\Administrator\Downloads\SQLServer2016-SSEI-Expr.exe
