## Create an inventory of software installed on a system
## Output the list to c:\inventory.txt

Get-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\* |  Select-Object DisplayName, DisplayVersion, Publisher, InstallDate | Format-Table –AutoSize >> c:\inventory.txt
