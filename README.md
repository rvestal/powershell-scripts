# powershell-scripts

My repository of powershell scripts I've used in my career. Feel free to use them at no charge, but please give me credit for the work.

* FindLargeFiles.ps1 - Search for large files in C:\Users folders
* software-inventory.ps1 - Gets the installed software of the local machine and exports it to a text file
* GPO-Backups.ps1 - This backs up all GPOs in a domain to a local folder
* add-dns.ps1 - Adds DNS entries from a CSV file
* example-newdnsrecords.csv - Example CSV file used by add-dns.ps1
* add-users.ps1 - Adds users to a local system or AD, sets password, and enables RDP
* example-users.txt - users.txt example for add-users.ps1
* delete-users.ps1 - Delete or Disable user accounts
* inventory.ps1 - Creates an inventory of software installed on a system and output to text file
* RemoveFilesOlderThan.ps1 - Removes files older than a specific period of days
* check-wmi-enabled.ps1 - Checks and verifies if WMI is enabled. If enabled, it will simply return the OS information about the system(s)
* reset-windows-updates.ps1 - Cleans a WSUS client software distribution database, restarts the services, and forces the client to reconnect to the WSUS server
* 2016-ciphers-and-tls-registry-settings.ps1 - Updates the registry keys for the Ciphers and Protocols to be in alignment with FIPS requirements. Disables all Ciphers except AES-128 and AES-256. Disables SSL2.0, SSL3.0, and TLSv1.0 clients and servers
* install-sql-server-2016.ps1 - Downloads and install Microsoft SQL Server Express 2016 and install with defaults, no interaction
* install-ssh.ps1 - Downloads and installs OpenSSH server, sets the firewall to the default port 22, and starts the service automatically
* remove-reinstall-wsus-service.ps1 - Remove and Reinstall WSUS server and required components. For a full rebuild. !!Recommend a backup first!!
* Update-for-ADV190013.ps1 - Updates Windows systems for the Microsoft Windows Server Registry Key Configuration Missing (ADV190013). Uses servers.txt
* check-updates.ps1 - checks a list of Micro$oft KB articles against a list of servers to determine if the list is installed. Special thanks to Mike F. Robbins for his initial script. https://mikefrobbins.com
* servers.txt - List of servers used by various scripts
