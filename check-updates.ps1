# This script checks for patches listed on for a list of servers
# Special thanks to Mike F. Robbins for his initial script that I've update to my needs.
# https://mikefrobbins.com/2017/05/18/use-powershell-to-determine-if-specific-windows-updates-are-installed-on-remote-servers/


# List of servers to check. You can remark this line out and change the computername argument
# in the next line to do just one server and bypass using the server list file
$serverName = Get-Content c:\servers.txt


Invoke-Command -ComputerName $serverName {
    $Patches = 'KB4494175', # Microsoft Windows Intel Microcode Updates
               'KB4503308', # Microsoft Windows Adobe Flash Player Security Update
               'KB4507460'  # Microsoft Windows Security Update
    Get-HotFix -Id $Patches
    Echo " "
}  -Credential (Get-Credential) -ErrorAction SilentlyContinue -ErrorVariable Problem 
foreach ($p in $Problem) {
    if ($p.origininfo.pscomputername) {
        Write-Warning -Message "Patch not found on $($p.origininfo.pscomputername)"
    }
    elseif ($p.targetobject) {
        Write-Warning -Message "Unable to connect to $($p.targetobject)"
    }
}
