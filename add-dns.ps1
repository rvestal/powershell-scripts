## This script adds entries from a csv file. The csv file requires:
## hostname, zone, record type, and IP address

Import-Csv -Path "C:\newdnsrecords.csv" | ForEach-Object { dnscmd.exe localhost /RecordAdd $_.zone $_.name /createPTR $_.type $_.IP }
