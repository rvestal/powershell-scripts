## This backs up all GPOs in a domain to a local folder.
## The script must be run from an administrative powershell

## Prompt for desired Path

$folderPath = Read-Host -Prompt 'Enter backup folder path: '

## For Debugging
## Write-Host $folderPath

## Test for path. If not found, create path and run backup
if (Test-Path -Path $folderPath)
{
Backup-GPO -All -Path $folderPath
}
else
{
mkdir $folderPath
Backup-GPO -All -Path $folderPath
