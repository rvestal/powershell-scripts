# This script removes and reinstalls WSUS and the software needed to 
# rebuild a server. It will set a temp pid.txt as a control PID.
# Each step can be run manually if preferred


# Test for pid.txt
#if (Test-Path c:\pid.txt) {

  #echo "ERROR!! PID EXISTS..EXITING!!"
  #exit

#else

  # Set PID for restart
  #echo date > c:\pid.txt

  # Uninstall Update Services and WID, then force restart
  # Uninstall-WindowsFeature -Name UpdateServices,Windows-Internal-Database -Restart


##
## AFTER REBOOT RESTART SCRIPT. IT SHOULD CONTINUE FROM HERE ##
##

# Verify c:\pid.txt is there. This would show that the system has removed the software
# Remove any old folders/files and reinstall software

# Test for pid.txt
#if (Test-Path c:\pid.txt) {

  #Remove c:\inetpub
  #if (Test-Path c:\inetpub) {
  #  Remove-Item -path c:\inetpub -Recurse -Confirm:$false
  #}

  # Remove C:\Windows\WID\*
  #Remove-Item -path c:\Windows\WID\* -Recurse -Confirm:$false

  # Remove c:\pid.txt to continue install
  #Remove-Item -path c:\pid.txt  -Confirm:$false

#}

#if (Test-Path c:\pid.txt) {

#echo "ERROR!! PID EXISTS..EXITING!!"
#exit

#else

# Install-WindowsFeature -Name UpdateServices,Windows-Internal-Database -Restart

#}

