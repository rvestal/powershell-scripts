## This script can either disable or delete a user account
## Just remove the pound sign (#) in front of the option you
## Would like to use


# delete users in c:\Users.txt
#Get-Content C:\Users.txt | ForEach-Object {net user $_ /delete}

# disable user account
#Get-Content C:\Users.txt | ForEach-Object {WMIC USERACCOUNT WHERE "Name='$_'" SET Disabled=True}
