# This script downloads and installs OpenSSH-Win64 to 
# C:\Program Files\OpenSSH, sets the default port 22
# In the Windows firewall and starts the service automatically

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$url = "https://github.com/PowerShell/Win32-OpenSSH/releases/download/v7.9.0.0p1-Beta/OpenSSH-Win64.zip"
$output = "c:\Users\Administrator\Downloads\OpenSSH-Win64.zip"
$start_time = Get-Date

Invoke-WebRequest -Uri $url -OutFile $output

Expand-Archive $output -DestinationPath "C:\Program Files"

ren "C:\Program Files\OpenSSH-Win64" "C:\Program Files\OpenSSH"

cd  "C:\Program Files\OpenSSH"

powershell.exe -ExecutionPolicy Bypass -File install-sshd.ps1

New-NetFirewallRule -Name sshd -DisplayName 'OpenSSH Server (sshd)' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 22

Start-Service sshd

"C:\Program Files\OpenSSH\FixHostFilePermissions.ps1"


Set-Service sshd -StartupType Automatic
